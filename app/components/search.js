import React from 'react';


export default (props) => (
  <div>
    <input onChange={({target}) => props.onChange(target.value)} type = "text" />
    <input onClick={props.onSearch} type = "submit" />
  </div>
)
