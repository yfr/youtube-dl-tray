import React from 'react';

export default ({info = {}}) => (
  <div>
    <img src={info.thumbnail} />
  </div>
)
