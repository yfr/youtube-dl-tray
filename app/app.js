import React from 'react';
import ReactDOM from 'react-dom';

const electron = window.require('electron');

// import Electron from 'electron';

import Search from './components/search.js'
import Info from './components/info.js'

let searchTerm = '';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};

    electron.ipcRenderer.on('asynchronous-message', (event, message) => {
        console.log(message);  // Prints "whoooooooh!"

        this.setState({
          info: message
        })
      });
  }

  componentWillReceiveProps() {
    this.state = {};
    this.setState(this.state);
  }
  onChange(value) {
    searchTerm = value;
  }

  onSearch(e) {
    e.preventDefault();

    electron.ipcRenderer.send('asynchronous-message', searchTerm);
  }

  render() {
    return (
      <div>
      <Search
        onChange={this.onChange}
        onSearch={this.onSearch} />
      <Info info={this.state.info} />
    </div>)
  }
}

// Render to ID content in the DOM
ReactDOM.render(
  <App /> ,
    document.querySelector('.content')
);
