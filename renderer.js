// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const youtubedl = require('youtube-dl')
const fs = require('fs')
const path = require('path')

const input = document.querySelector('input')
const button = document.querySelector('button')

button.addEventListener('click', e => {
    var video = youtubedl(input.value, [], {
        cwd: path.join(process.env['HOME'], 'desktop')
    });

    // Will be called when the download starts.
    video.on('info', function(info) {
        console.log(info);
        console.log('Download started');
        console.log('filename: ' + info._filename);
        console.log('size: ' + info.size);

        video.pipe(fs.createWriteStream(path.join(process.env['HOME'], 'desktop', info._filename)));
    });
})
